
 
 @slpdvca
Feature: DVCA USER VALIDATIONS ON SLP

    Description of feature goes here
 
  Scenario: As a DVCA Administrator I should see the Student Load Planner Tools Slide Out Menu displaying automatically
    Given   I am a DVCA User
    When I am in Student Load Planning Program View
    Then I should have the Student Load Planner Tools Slide Out Menu display automatically
    And I should not see a User Profile Your pre-selected programs drop down or button under these titles


    
  Scenario: As a DVCA Administrator I should be able to change any Faculty
    Given I am a DVCA User
    And I am in Student Load Planning Program View
    When I select any Faculty "1006"
    And I change a number
    And I click Save
    Then the numbers should remain and the yellow background should no longer be displayed