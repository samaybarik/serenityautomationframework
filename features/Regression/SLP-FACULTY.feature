@slpfaculty
Feature: FACULTY VALIDATIONS ON SLP

  Description of feature goes here



  Scenario: As a Faculty User I should be able to confirm the changes made with Flow Through to Continuing Table
    Given I am a SLP Faculty User
    And I am in Student Load Planning Program View
    When I change a Faculty Headcount
    And I change a Faculty EFTSL
    And I click Flow Through to Continuing
    And I check the Flow Through to Continuing Table
    And I click Proceed
    Then the changed values should match the Flow Through to Continuing Table
    And all changes should be displayed with a Yellow Background



  Scenario: FP -TP1: As a Faculty User I should be able to change a Student Category and see the data automatically update
    Given I am a SLP Faculty User
    And I am in Student Load Planning Program View
    When I change a Student Category Tab to "Fee Paying"
    And Select "Teaching Period One" sub tab
    Then I should see the correct table is loaded


  Scenario:  FP -TP2: As a Faculty User I should be able to change a Student Category and see the data automatically update
    Given I am a SLP Faculty User
    And I am in Student Load Planning Program View
    When I change a Student Category Tab to "Fee Paying"
    And Select "Teaching Period Two" sub tab
    Then I should see the correct table is loaded


  Scenario: FP -TP3: As a Faculty User I should be able to change a Student Category and see the data automatically update
    Given I am a SLP Faculty User
    And I am in Student Load Planning Program View
    When I change a Student Category Tab to "Fee Paying"
    And Select "Teaching Period Three" sub tab
    Then I should see the correct table is loaded



  Scenario: INT -TP1: As a Faculty User I should be able to change a Student Category and see the data automatically update
    Given I am a SLP Faculty User
    And I am in Student Load Planning Program View
    When I change a Student Category Tab to "International"
    And Select "Teaching Period One" sub tab
    Then I should see the correct table is loaded


  Scenario: INT -TP2: As a Faculty User I should be able to change a Student Category and see the data automatically update
    Given I am a SLP Faculty User
    And I am in Student Load Planning Program View
    When I change a Student Category Tab to "International"
    And Select "Teaching Period Two" sub tab
    Then I should see the correct table is loaded


  Scenario: INT -TP3: As a Faculty User I should be able to change a Student Category and see the data automatically update
    Given I am a SLP Faculty User
    And I am in Student Load Planning Program View
    When I change a Student Category Tab to "International"
    And Select "Teaching Period Three" sub tab
    Then I should see the correct table is loaded


  Scenario: OTH -TP1: As a Faculty User I should be able to change a Student Category and see the data automatically update
    Given I am a SLP Faculty User
    And I am in Student Load Planning Program View
    When I change a Student Category Tab to "Other"
    And Select "Teaching Period One" sub tab
    Then I should see the correct table is loaded


  Scenario: OTH -TP2: As a Faculty User I should be able to change a Student Category and see the data automatically update
    Given I am a SLP Faculty User
    And I am in Student Load Planning Program View
    When I change a Student Category Tab to "Other"
    And Select "Teaching Period Two" sub tab
    Then I should see the correct table is loaded


  Scenario: OTH -TP3: As a Faculty User I should be able to change a Student Category and see the data automatically update
    Given I am a SLP Faculty User
    And I am in Student Load Planning Program View
    When I change a Student Category Tab to "Other"
    And Select "Teaching Period Three" sub tab
    Then I should see the correct table is loaded


  Scenario: CGS -TP1: As a Faculty User I should be able to change a Student Category and see the data automatically update
    Given I am a SLP Faculty User
    And I am in Student Load Planning Program View
    When I change a Student Category Tab to "Commonwealth Grant Scheme"
    And Select "Teaching Period One" sub tab
    Then I should see the correct table is loaded


  Scenario: CGS -TP2: As a Faculty User I should be able to change a Student Category and see the data automatically update
    Given I am a SLP Faculty User
    And I am in Student Load Planning Program View
    When I change a Student Category Tab to "Commonwealth Grant Scheme"
    And Select "Teaching Period Two" sub tab
    Then I should see the correct table is loaded


  Scenario: CGS -TP3: As a Faculty User I should be able to change a Student Category and see the data automatically update
    Given I am a SLP Faculty User
    And I am in Student Load Planning Program View
    When I change a Student Category Tab to "Commonwealth Grant Scheme"
    And Select "Teaching Period Three" sub tab


  Scenario: As a Faculty User when I change a value and leave the page I should receive a warning telling me that the data will not be saved
    Given I am a SLP Faculty User
    And I am in Student Load Planning Program View
    When I change an Editable Field
    And I leave the page
    Then I should see an Alert Message informing me that my data will not be saved




  Scenario: As a Faculty User when I toggle a Pin to current tab switch on I should not be able to change the Tab, and the other Tab should not affect this tab
    Given I am a SLP Faculty User
    And I am in Student Load Planning Program View
    When I change a Teaching Period Tab
    And I toggle the Pin current tab for Teaching Period
    And I change a Student Category Tab
    Then I cannot change the Teaching Period Tab
    And the Teaching Period Tab is Yellow




  Scenario: As a Faculty User I should be able to see the values for other faculties but I should not be able to change others than my own
    Given I am a SLP Faculty User
    And I am in Student Load Planning Program View
    When I display the Student Load Planner Tools Slide Out Menu
    And I select a Program not in my Faculty
    Then I should not be able to edit the value of Faculty Headcount or Faculty EFTSL
    And I should not see the Pin current tab toggles at the end of tabs


  Scenario: As a Faculty User I should be able to select a Pre-selected programs
    Given I am a SLP Faculty User
    And I am in Student Load Planning Program View
    When I display the Student Load Planner Tools Slide Out Menu
    And I select a Program from Pre-selected programs
    And I click the Go button next to Pre-selected programs
    Then I should see the numbers for the selected drop down
    And I should be able to edit the numbers
    And I should see the Pin current tab toggles


  Scenario: As a Faculty User when I am on the lowest numerical program in my Faculties I should not see a Previous Button
    Given I am a SLP Faculty User
    And I am in Student Load Planning Program View
    When I look for Previous and Next Buttons
    Then I should not see a Previous button
    And I should see a Next button with the next numerical Program


  Scenario: As a Faculty User when I am on the highest numerical program in my Faculties I should not see a Next button
    Given I am a SLP Faculty User
    And I am in Student Load Planning Program View
    When I display the Student Load Planner Tools Slide Out Menu
    When I select the last program from the Pre-selected programs
    And I click the Go button next to Pre-selected programs
    Then I should not see a Next button
    And I should see a Previous button with he previous numerical Program


  Scenario: As a Faculty User when I click Next the Program associated with the Next button should match what is displayed
    Given I am a SLP Faculty User
    And my Faculty has more than one Program
    And I am in Student Load Planning Program View
    When I click on Next
    Then the program associated with the displayed number on next button should be displayed on the page
    And the Previous button should display the program last displayed


  Scenario: As a Faculty User when I click Previous the Program associated with the Previous button should match what is displayed
    Given I am a SLP Faculty User
    And my Faculty has more than one Program
    And I am in Student Load Planning Program View
    When I click on Previous
    Then the program associated with the displayed number on prev button should be displayed on the page
    And the Next button should display the program last displayed



  Scenario: As a Faculty User when I search a non-existent Program Code then I should receive an Error Message
    Given I am a SLP Faculty User
    And I am in Student Load Planning Program View
    When I use Student Load Planner Tools to search an invalid Program Code "50987"
    And I select teaching periods
    Then I should receive an error message /Program "50987" does not exist. Please rectify.\?/


  Scenario: As a Faculty User when I click Cancel -> No after changes I should receive a pop up warning about it, then I should see the changes not remain
    Given I am a SLP Faculty User
    And I am in Student Load Planning Program View
    When I change a field
    And I click Cancel
    Then I should have a Confirm your request popup appear
    And  I click no
    Then the change should remain



  Scenario: As a Faculty User when I click Cancel after changes->Yes I should receive a pop up warning about it, then I should see the changes remain
    Given I am a SLP Faculty User
    And I am in Student Load Planning Program View
    When I change a field
    And I click Cancel
    Then I should have a Confirm your request popup appear
    And  I click yes
    Then the change should revert


  Scenario: Change view according to faculty selected
    Given I am a SLP Faculty User
    And I am in Student Load Planning Program View
    And I am on 3 Year Plan page
    When I search for a faculty which is not my own "Faculty of Science"
    Then The 3 year plan page for "Faculty of Science" is displayed
    And 3 Year Plan page is read only


  Scenario: Leaving the 3 Year Plan page with unsaved data warning
    Given I am a SLP Faculty User
    And I am in Student Load Planning Program View
    And I am on 3 Year Plan page
    And I have modified the projection to any random value
    When I navigate Home
    Then I receive an alert stating I have unsaved data

  Scenario: Comments can be saved in 3 Year Plan page
    Given I am a SLP Faculty User
    And I am in Student Load Planning Program View
    And I am on 3 Year Plan page
    And I enter a comment "This is a new comment" in the comment modal
    When I click the Save and Close button
    Then I should be on the 3 Year Plan page
    And I can see the comment "This is a new comment" is present in the modal






