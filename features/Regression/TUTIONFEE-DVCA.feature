@tutionfeedvca 
Feature: DVCA USER VALIDATION ON TUTION FEE

    Description of feature goes here

@wip2
    Scenario: I would like the actual percentage calculated by formulae test 2
        Given I am a DVCA User
        And I am on the Tuition Fee Setting page
        When I change the percentage value to "200"
        Then Actual Percentage is calculated is "200.19"

@wip2
    Scenario: Relabel the new 'delete' button to read 'remove proposal'
        Given I am a DVCA User
        When I am on the Tuition Fee Setting page
        Then "Remove Proposal" button exists

@wip2
    Scenario: I would like to access a Tuition Fee Setting Admin Page
        Given I am a DVCA User
        And I am on the Tuition Fee Admin page
        When I click Tuition Fee Approvals Enter section button
        Then the Tuition Fee Setting Admin page is displayed

@wip2
    Scenario: New proposal "action" button
        Given I am a DVCA User
        And I am on the Tuition Fee Administration Proposal page
        When I click on new proposal action button
        Then Tuition Fee Setting Administration for New Proposals page is loaded
        And "Reject Proposal" action button exists
        And "Approve Proposal" action button exists

    Scenario: Cancel new proposal action
        Given I am a DVCA User
        And I am on Tuition Fee Setting Administration for New Proposals page
        When I click on the "Cancel" button
        Then the Tuition Fee Setting Admin page is displayed

    Scenario: Cancel proposed deletion action
        Given I am a DVCA User
        And I am on Tuition Fee Setting Administration for Proposed Deletions page
        When I click on the "Cancel" button
        Then the Tuition Fee Setting Admin page is displayed

    Scenario: Edit fields on Tuition Fee Setting Administration page
        Given I am a DVCA User
        When I am on Tuition Fee Setting Administration for New Proposals page
        Then I should be able to edit all fields except faculty comment field

    Scenario: view proposal "history"
        Given I am a DVCA User
        And I am on the Tuition Fee Administration Proposal page
        When I click on history link
        Then new proposals history heading is visible
