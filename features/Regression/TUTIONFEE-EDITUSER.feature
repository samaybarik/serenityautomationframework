@tutionfeeEDITUSER
Feature:  Tuition Fee Edit User VALIDATION ON TUTION FEE

    Description of feature goes here

    Scenario: I would like the actual percentage calculated by formulae
        Given I am a Tuition Fee Edit User
        And I am on the Tuition Fee Setting page
        When I change the percentage value to "100"
        Then Actual Percentage is calculated is "99.81"

    Scenario: dvca eftsl is displayed
        Given I am a Tuition Fee Edit User
        When I am on the Tuition Fee Setting page
        Then "2018 EFTSL" heading is displayed

    Scenario: I would like to change the order of drop down fields in the modal
        Given I am a Tuition Fee Edit User
        And I am on the Tuition Fee Setting page
        When I click on the "new-proposal" button
        Then the "new-proposals" modal is displayed


