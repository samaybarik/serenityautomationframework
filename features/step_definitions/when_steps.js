'use strict';

const tuitionFeeSettings = require('@page_objects/TuitionFee/tuition.fee.setting.page');
const whereWouldYouLikeToGoPage = require('@page_objects/where.would.you.like.to.go.page');
const commonTopNavBar = require('@page_objects/common.top.nav.bar');
const tuitionFeeAdmin = require('@page_objects/TuitionFee/tuition.fee.admin.page');
const tuitionFeeApproval = require('@page_objects/TuitionFee/tuition.fee.approval.page');
const commonPage = require('@page_objects/common.page');
const threeYearPlanPage = require('@page_objects/SLP/slp.three.year.plan.page');
const programTablePage = require('@page_objects/SLP/programtable.component');
const slpToolsPage = require('@page_objects/SLP/slptools.component');
const buffer = require('@helper/buffer');


module.exports = function whenSteps() {

  this.When(/^I change the percentage value to "([^"]*)"$/, function (updatedPercent) {

    tuitionFeeSettings.enterIncreasePercentage(updatedPercent);

  });

  this.When(/^I click Tuition Fee Approvals Enter section button$/, function () {
    tuitionFeeAdmin.goToApprovalAdmin();
  });

  this.When(/^I click on the "([^"]*)" button$/, function (buttonName) {
    commonPage.clickButton(buttonName);
  });

  this.When(/^I click on new proposal action button$/, function () {
    tuitionFeeApproval.clickActionNewProposalButton();
  });

  this.When(/^I click on delete proposal action button$/, function () {
    tuitionFeeApproval.clickActionDeleteProposalButton();
  });

  this.When(/^I click the Save and Close button$/, function () {
    threeYearPlanPage.saveComment();
  });
  this.When(/^I navigate Home$/, function () {
    commonTopNavBar.navigateToHome();
  });

  this.When(/^I search for a faculty which is not my own "([^"]*)"$/, function (readOnlyFaculty) {
    threeYearPlanPage.selectReadOnlyFaculty(readOnlyFaculty);
  });

  this.When(/^I click on history link$/, function () {
    tuitionFeeApproval.clickHistoryLink();
  });

  this.When(/^I change a Faculty Headcount$/, async function () {
    await programTablePage.selectSubTabTeachingPeriodOne();
    programTablePage.setRow('0-48', 'New to UNSW Academic Career');
    await programTablePage.setFacultyHeadCount(Math.floor(Math.random() * (30 - 1) + 1));

  });
  this.When(/^I change a Faculty EFTSL$/, function () {

    programTablePage.setFacultyEftsl(Math.floor(Math.random() * (10 - 1) + 1));
  });
  this.When(/^I click Flow Through to Continuing$/, function () {
    programTablePage.clickFlowThruContinuing();
  });
  this.When(/^I check the Flow Through to Continuing Table$/, async function () {
    await programTablePage.validateFlowThruChangesTableIsDisplayed();
    await programTablePage.readChangedValues();
  });
  this.When(/^I click Proceed$/, function () {
    programTablePage.clickProceedButton();
  });
  this.When(/^I change a Student Category Tab to "([^"]*)"$/, function (tabValue) {
    switch (tabValue) {
      case 'Fee Paying':
        programTablePage.selectTabFeePaying();
        break;
      case 'International':
        programTablePage.selectTabInternational();
        break;
      case 'Other':
        programTablePage.selectTabOther();
        break;
      case 'Commonwealth Grant Scheme':
        programTablePage.selectTabCommonWealthGrantScheme();
        break;

      default:
        console.log('No matching values for tab');
    }
  });

  this.When(/^Select "([^"]*)" sub tab$/, function (subTabValue) {
    console.log('subTabValue:' + subTabValue);
    switch (subTabValue) {

      case 'Teaching Period One':
        programTablePage.selectSubTabTeachingPeriodOne();
        break;
      case 'Teaching Period Two':
        programTablePage.selectSubTabTeachingPeriodTwo();
        break;
      case 'Teaching Period Three':
        programTablePage.selectSubTabTeachingPeriodThree();
        break;

      default:
        console.log('No matching values for subtab');


    }
  });

  this.When(/^I change an Editable Field$/, async function () {
    await   programTablePage.selectSubTabTeachingPeriodOne();
    programTablePage.setRow('0-48', 'New to UNSW Academic Career');
    await programTablePage.editFacultyHeadCount(Math.floor(Math.random() * (30 - 1) + 1));
  });


  this.When(/^I leave the page$/, function () {
    commonTopNavBar.navigateToThreeYearPlan();
  });
  this.When(/^I change a Teaching Period Tab$/, function () {
    programTablePage.selectSubTabTeachingPeriodTwo();
  });

  this.When(/^I toggle the Pin current tab for Teaching Period$/, function () {

    programTablePage.pinCurrentTab();
  });

  this.When(/^I change a Student Category Tab$/, function () {
    programTablePage.selectTabFeePaying();
  });

  this.When(/^I display the Student Load Planner Tools Slide Out Menu$/, function () {
    slpToolsPage.clickSlideIcon();
  });
  this.When(/^I select a Program not in my Faculty$/, function () {
    // Write code here that turns the phrase above into concrete actions
    slpToolsPage.setProgramCode("1004");
    slpToolsPage.searchProgram();
  });

  this.When(/^I select a Program from Pre\-selected programs$/, function () {
    slpToolsPage.selectProgramUnderUserProfile('1650 - Computer Science and Eng');
  });

  this.When(/^I click the Go button next to Pre\-selected programs$/, function () {
    slpToolsPage.clickGoButtonUnderUserProfile();
  });

  this.When(/^I look for Previous and Next Buttons$/, function () {

  });
  this.When(/^I select the last program from the Pre\-selected programs$/, function () {
    slpToolsPage.selectLastProgramUnderUserProfile();
  });

  this.When(/^I click on Next$/, function () {
    programTablePage.getProgramCodeOnPage();
    programTablePage.getPgmCodeOnNextButton();;
    programTablePage.clickNextButton();
  });

  this.When(/^I click on Previous$/, function () {
    programTablePage.clickNextButton();
    programTablePage.getProgramCodeOnPage();
    programTablePage.getPgmCodeOnPrevButton();
    programTablePage.clickPrevButton();
  });

  this.When(/^I use Student Load Planner Tools to search an invalid Program Code "([^"]*)"$/, function (invalidCode) {
    slpToolsPage.clickSlideIcon();
    slpToolsPage.setProgramCode(invalidCode);
  });

  this.When(/^I select teaching periods$/, function () {
    slpToolsPage.searchProgram();
  });

  this.When(/^I change a field$/, async function () {
    await programTablePage.selectSubTabTeachingPeriodOne();
    programTablePage.setRow('0-48', 'New to UNSW Academic Career');
    let oldHeadCount = await programTablePage.getFacultyHeadCount();
    buffer.setBuffer("oldFacultyCount", oldHeadCount);
    buffer.setBuffer("facultyHC", Math.floor(Math.random() * (30 - 1) + 1));
    programTablePage.setFacultyHeadCount(buffer.getBuffer("facultyHC"));

  });

  this.When(/^I click Cancel$/, function () {
    programTablePage.clickCancelButton();
  });

  this.When(/^I select any Faculty "([^"]*)"$/, function (arg1) {
    slpToolsPage.setProgramCode(arg1);
    slpToolsPage.setTeachingPeriod("Teaching Period One");
    slpToolsPage.searchProgram();
  });

  this.When(/^I change a number$/, async function () {
    await programTablePage.selectSubTabTeachingPeriodOne();
    programTablePage.setRow('0-48', 'New to UNSW Academic Career');
    buffer.setBuffer("facultyHC", Math.floor(Math.random() * (30 - 1) + 1));
    await programTablePage.setFacultyHeadCount(buffer.getBuffer("facultyHC"));
  });

  this.When(/^I click Save$/, async function () {
    console.log("A");
   await  programTablePage.clickSaveButton();
   console.log("B");
  });


};



