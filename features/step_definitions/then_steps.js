'use strict';

const tuitionFeeSettingPage = require('@page_objects/TuitionFee/tuition.fee.setting.page');
const commonPage = require('@page_objects/common.page');
const tuitionFeeApprovalPage = require('@page_objects/TuitionFee/tuition.fee.approval.page');
const tuitionFeeAdminNewProposalActionPage = require('@page_objects/TuitionFee/tuition.fee.admin.new.proposal.action.page');
const tuitionFeeAdminDeleteProposalActionPage = require('@page_objects/TuitionFee/tuition.fee.admin.delete.proposal.action.page');
const threeYearPlanPage = require('@page_objects/SLP/slp.three.year.plan.page');
const programTablePage = require('@page_objects/SLP/programtable.component');
const slpToolsPage = require('@page_objects/SLP/slptools.component');
const commonTopNavBar = require('@page_objects/common.top.nav.bar');
var buffer = require('@helper/buffer');


module.exports = function thenSteps() {

  this.Then(/^Actual Percentage is calculated is "([^"]*)"$/, function (actualIncreasePercent) {

    tuitionFeeSettingPage.assertActualIncrease(actualIncreasePercent);

  });

  this.Then(/^"([^"]*)" heading is displayed$/, function (expectedEftslColumn) {

    tuitionFeeSettingPage.assertEftslColumn(expectedEftslColumn);

  });

  this.Then(/^the Tuition Fee Setting Admin page is displayed$/, function () {
    tuitionFeeApprovalPage.headingExists('Tuition Fee Setting Administration');
  });

  this.Then(/^the "([^"]*)" modal is displayed$/, function (modalName) {
    commonPage.modalDisplay(modalName);
  });

  this.Then(/^"([^"]*)" button exists$/, function (buttonName) {
    tuitionFeeSettingPage.buttonExists(buttonName);
  });

  this.Then(/^Tuition Fee Setting Administration for New Proposals page is loaded$/, function () {
    tuitionFeeAdminNewProposalActionPage.assertHeading('New Proposals');
  });
  this.Then(/^Tuition Fee Setting Administration for Delete Proposals page is loaded$/, function () {
    tuitionFeeAdminDeleteProposalActionPage.assertHeading('Proposed Deletions');
  });

  this.Then(/^"([^"]*)" action button exists$/, function (buttonName) {
    tuitionFeeAdminNewProposalActionPage.buttonExists(buttonName);
  });
  this.Then(/^"([^"]*)" delete action button exists$/, function (buttonName) {
    tuitionFeeAdminDeleteProposalActionPage.buttonExists(buttonName);
  });

  this.Then(/^I should be able to edit all fields except faculty comment field$/, function () {
    tuitionFeeAdminNewProposalActionPage.assertEditableFields();
  });

  this.Then(/^I should be on the 3 Year Plan page$/, function () {
    threeYearPlanPage.assertPageTitle();
  });

  this.Then(/^I receive an alert stating I have unsaved data$/, function () {
    threeYearPlanPage.assertAlertText();
  });

  this.Then(/^The 3 year plan page for "([^"]*)" is displayed$/, function (readOnlyFaculty) {
    threeYearPlanPage.assertFacultyPage(readOnlyFaculty);
  });

  this.Then(/^new proposals history heading is visible$/, function () {
    tuitionFeeApprovalPage.historyExists();
  });

  this.Then(/^the changed values should match the Flow Through to Continuing Table$/, async function () {
    commonPage.disableBrowserPopUp();
    await programTablePage.selectSubTabTeachingPeriodThree();
    await programTablePage.setRow('0-48', 'Continuing');
    await programTablePage.validateSuccessMessage('The following Teaching Periods have now been updated: Teaching Period One to Teaching Period Three.');
    await programTablePage.validateHeadCountValue();
    //await programTablePage.validateEftslValue();

  });

  this.Then(/^all changes should be displayed with a Yellow Background$/, function () {
    commonPage.disableBrowserPopUp();
    programTablePage.validateHeadCountBackGroundColor();
    programTablePage.validateEftslBackgroundColor();
   
  });

  this.When(/I should see the correct table is loaded$/, function () {
    programTablePage.validateCorrectTableLoaded();
  });

  this.Then(/^I should see an Alert Message informing me that my data will not be saved$/, function () {

    programTablePage.validateLeaveAlertPresent();

  });
  this.Then(/^I cannot change the Teaching Period Tab$/, function () {
    programTablePage.validateTeachingPeriodOneDisabled();
  });
  this.Then(/^the Teaching Period Tab is Yellow$/, function () {
    programTablePage.getBackGroundColorOfTeachingPeriodTwoTab();
    programTablePage.pinCurrentTab();

  });

  this.Then(/^I should have the Student Load Planner Tools Slide Out Menu display automatically$/, function () {
    slpToolsPage.isProgramFormDisplayed();
  });

  this.Then(/^I should not see a User Profile Your pre\-selected programs drop down or button under these titles$/, function () {
    slpToolsPage.validateUserProfileSectionNotDisplayed();
    slpToolsPage.validateAvailableFacultiesSectionNotDisplayed();

  });

  this.Then(/^I should not be able to edit the value of Faculty Headcount or Faculty EFTSL$/, async function () {
    await programTablePage.selectTabCommonWealthGrantScheme();
    await programTablePage.selectSubTabTeachingPeriodOne();
    await programTablePage.setRow('0-48', 'New to UNSW Academic Career');
    await programTablePage.validateFacultyHeadCountIsReadOnly();
    await programTablePage.validateFacultyEFTSLIsReadOnly();
  });

  this.Then(/^I should not see the Pin current tab toggles at the end of tabs$/, function () {
    programTablePage.validatePinCurrentTabOptionIsNotVisible();
  });

  this.Then(/^I should see the numbers for the selected drop down$/, function () {

  });

  this.Then(/^I should be able to edit the numbers$/, async function () {
    await programTablePage.selectSubTabTeachingPeriodOne();
    await programTablePage.setRow('0-48', 'New to UNSW Academic Career');
    await programTablePage.setFacultyHeadCount(21);
    await programTablePage.setFacultyEftsl(4);

  });

  this.Then(/^I should see the Pin current tab toggles$/, function () {
    programTablePage.pinCurrentTab();
    commonPage.disableBrowserPopUp();

  });

  this.Then(/^I should not see a Previous button$/, function () {
    programTablePage.validateprevButtonIsNotVisible();
  });

  this.Then(/^I should see a Next button with the next numerical Program$/, function () {
    programTablePage.validateNextButtonContainsNextNumericalProgram();
  });


  this.Then(/^I should not see a Next button$/, function () {
    programTablePage.validateNextButtonIsNotVisible();
  });

  this.Then(/^I should see a Previous button with he previous numerical Program$/, function () {
    programTablePage.validatePrevButtonContainsPrevNumericalProgram();
  });

  this.Then(/^the program associated with the displayed number on next button should be displayed on the page$/, function () {
    programTablePage.validateProgramCodeOnPageMatchesNextButtonValInPrevScreen();
  });

  this.Then(/^the Previous button should display the program last displayed$/, function () {
    programTablePage.validateProgramCodeOnPrevButtonValMatchesPageCodeInPrevScreen();
  });

  this.Then(/^the program associated with the displayed number on prev button should be displayed on the page$/, function () {
    programTablePage.validateProgramCodeOnPageMatchesPrevButtonValInPrevScreen();
  });

  this.Then(/^the Next button should display the program last displayed$/, function () {
    programTablePage.validateProgramCodeOnNextButtonValMatchesPageCodeInPrevScreen();
  });
  this.Then(/^I should receive an error message \/Program "([^"]*)" does not exist\. Please rectify\.\\\?\/$/, function (message) {

    programTablePage.validateErrorMessage(message);
  });

  this.Then(/^I should have a Confirm your request popup appear$/, function () {
    programTablePage.validateCancelPopupAppears();
  });
  this.Then(/^I click no$/, function () {
    programTablePage.clickNoButtonOnCancelPopup();
  });

  this.Then(/^the change should remain$/, function () {

    programTablePage.validateFacultyHeadCount(`${buffer.getBuffer("facultyHC")}`);
    commonPage.disableBrowserPopUp();
  });

  this.Then(/^I click yes$/, function () {

    programTablePage.clickYesButtonOnCancelPopup();

  });

  this.Then(/^the change should revert$/, function () {
    programTablePage.validateFacultyHeadCount(buffer.getBuffer("oldFacultyCount"));
    commonPage.disableBrowserPopUp();
  });
  this.Then(/^the numbers should remain and the yellow background should no longer be displayed$/, function () {
    programTablePage.validateFacultyHeadCount(`${buffer.getBuffer("facultyHC")}`);
  });

};

