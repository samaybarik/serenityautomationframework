'use strict';

const expect = require('@helper/expect').expect;
var EC = protractor.ExpectedConditions;

// const labelStudentsPlanner = element(by.xpath("//h4[.='Student Load Planner Tools']/following-sibling::ul"));

const slpToolsPage = element(by.id('slp-tools'));

const slideIcon= element(by.xpath(`//button[@data-target='.navmenu']`));

const slpToolsProgramForm = {
  label: element(by.xpath("//h4[.='Student Load Planner Tools']")),
  programCode: element(by.id('quick_link_program')),
  teachingPeriod: element(by.id('quick_link_teachingPeriods')),
  serachButton: element(by.id('prog-search-button')),
}
  ;

const userProfileForm = {
  userProfileBlock: element(by.xpath(`//label[@for='quick_link_userProfilePrograms']/ancestor::ul`)),
  label: element(by.xpath("//h4[.='User Profile']")),
  labelPreSelectedProgram: element(by.xpath('//p[.=\'Your pre-selected programs\']')),
  programsSelectList: element(by.id('quick_link_userProfilePrograms')),
  goButton: element(by.id('user-profile-go')),
};
const facultiesForm = {
  userFacultiesBlock: element(by.xpath(`//label[@for='quick_link_userFaculties']/ancestor::ul`)),
  label: element(by.xpath("//h4[.='Available Faculties']")),
  facultiesSelectList: element(by.id('quick_link_userFaculties')),
  goButton: element(by.id('user-faculty-go')),
};

const slpTools = function () {

  this.clickSlideIcon=function(){
    slideIcon.click();
  }

  this.isProgramFormDisplayed = function () {
    expect(slpToolsPage.isDisplayed()).to.eventually.be.true;
  };
  this.setProgramCode = function (code) {
    slpToolsProgramForm.programCode.clear();
    slpToolsProgramForm.programCode.sendKeys(code);
  };
  this.setTeachingPeriod=function(option){
    slpToolsProgramForm.teachingPeriod.element(by.cssContainingText('option', `${option}`)).click();
  }
  this.searchProgram = function () {
    slpToolsProgramForm.serachButton.click();
  };

  this.selectProgramUnderUserProfile = async function (program) { 
    userProfileForm.programsSelectList.element(by.cssContainingText('option', `${program}`)).click();
  };

  this.selectLastProgramUnderUserProfile = async function () { 
    userProfileForm.programsSelectList.all(by.tagName('option')).last().click();
  };
  this.clickGoButtonUnderUserProfile = function () {
    userProfileForm.goButton.click();
  };
  this.selectAvailableFaculties = function (faculties) {
    facultiesForm.facultiesSelectList.element(by.cssContainingText(`'option', '${faculties}'`)).click();
  };
  this.clickGoButtonUnderAvailableFaculties = function () {
    facultiesForm.goButton.click();
  };

  this.validateUserProfileSectionNotDisplayed = function () {
    browser.wait(EC.visibilityOf(slpToolsPage), 5000);
    // browser.sleep(5000);

    expect(userProfileForm.userProfileBlock.isPresent()).to.eventually.be.false;
  };

  this.validateAvailableFacultiesSectionNotDisplayed = function () {

    expect(facultiesForm.userFacultiesBlock.isDisplayed()).to.eventually.be.false;

  };


};

module.exports = new slpTools();
