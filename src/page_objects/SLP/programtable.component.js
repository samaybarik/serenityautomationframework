'use strict';


const commonTopNavBar = require('@page_objects/common.top.nav.bar');
const expect = require('@helper/expect').expect;
const EC = protractor.ExpectedConditions;

let mainTab;
const mainTabs = {
  tabCommonWealthGrantScheme: element(by.xpath('//a[@class=\'maintabs pin \'][text()=\'Commonwealth Grant Scheme\']')),
  tabFeePaying: element(by.xpath('//a[@class=\'maintabs pin \'][text()=\'Fee Paying\']')),
  tabInternational: element(by.xpath('//a[@class=\'maintabs pin \'][text()=\'International\']')),
  tabOther: element(by.xpath('//a[@class=\'maintabs pin \'][text()=\'Other\']')),
  tabTotal: element(by.css('a.maintabs.totalTab'))
}
// const tabCommonWealthGrantScheme = element(by.xpath('//a[@class=\'maintabs pin \'][text()=\'Commonwealth Grant Scheme\']'));
// const tabFeePaying = element(by.xpath('//a[@class=\'maintabs pin \'][text()=\'Fee Paying\']'));
// const tabInternational = element(by.xpath('//a[@class=\'maintabs pin \'][text()=\'International\']'));
// const tabOther = element(by.xpath('//a[@class=\'maintabs pin \'][text()=\'Other\']'));
// const tabTotal = element(by.css('a.maintabs.totalTab'));

let subTab;
let destId;
let tableId;

const subTabs = {
   subTabTeachingPeriodOne : element(by.xpath('//li[contains(@class,\'active\')]/..//a[text()=\'Teaching Period One\']/..')),
   subTabTeachingPeriodOneLink : element(by.xpath('//li[contains(@class,\'active\')]/..//a[text()=\'Teaching Period One\']')),
   subTabTeachingPeriodTwoLink : element(by.xpath('//li[contains(@class,\'active\')]/..//a[text()=\'Teaching Period Two\']')),
   subTabTeachingPeriodThreeLink : element(by.xpath('//li[contains(@class,\'active\')]/..//a[text()=\'Teaching Period Three\']')),
   subTabTotal : element(by.xpath('//li[@class=\'active\'][a[@class=\'subnav-item pin \']]/..//a[text()=\'Total\']'))
}
// const subTabTeachingPeriodOne = element(by.xpath('//li[contains(@class,\'active\')]/..//a[text()=\'Teaching Period One\']/..'));
// const subTabTeachingPeriodOneLink = element(by.xpath('//li[contains(@class,\'active\')]/..//a[text()=\'Teaching Period One\']'));
// const subTabTeachingPeriodTwoLink = element(by.xpath('//li[contains(@class,\'active\')]/..//a[text()=\'Teaching Period Two\']'));
// const subTabTeachingPeriodThreeLink = element(by.xpath('//li[contains(@class,\'active\')]/..//a[text()=\'Teaching Period Three\']'));
// const subTabTotal = element(by.xpath('//li[@class=\'active\'][a[@class=\'subnav-item pin \']]/..//a[text()=\'Total\']'));

const flowThruChangesTable = element(by.css('#flow-through-program .modal-header'));
const proceedButton = element(by.css('#flow-through-program .modal-footer button[type=\'Submit\']'));

var uoc;
var status;
const targetHCTeachingPeriodThree = element(by.xpath("//td[.='Teaching Period Three']/following-sibling::td[3]"));
let targetHCTeachingPeriodThreeValue;
const targetEftslTeachingPeriodThree = element(by.xpath("//td[.='Teaching Period Three']/following-sibling::td[5]"));
let targetEftslTeachingPeriodThreeValue;
//let pinCurrentTabElement = element(by.id('INTL_pin-subtab'));
let pinCurrentTabElement = element(by.id('DEST_pin-subtab'));
let nextButton = element(by.xpath(`//i[@class='fa fa-arrow-right']/..`));
let prevButton = element(by.xpath(`//i[@class='fa fa-arrow-left']/..`));
let pgmCodeOnPage = element(by.xpath("//h2[.='Program View']/following-sibling::h3"));
let pgmCodeOnNextButtonValue;
let pgmCodeOnPrevButtonValue;
let pgmCodeOnPageValue;
let errorMessage = element(by.css(".alert"));
let cancelButton = element(by.id("submitBtnTop"));
let cancelPopup = {
  popup: element(by.id("confirm-cancel-model")),
  yesButton: element(by.css("#confirm-cancel-model .modal-footer button.next")),
  noButton: element(by.css("#confirm-cancel-model .modal-footer button.btn-danger"))
}
let saveButton = element(by.id("saveButtonTop"));
let pgmCommentsModal = {
  modal: element(by.id("percentage-comments")),
  commentsArea: element(by.id("percentage-comments-area")),
  saveButton: element(by.id("percentage-save"))
}




const ProgramTable = function () {

  this.selectTabCommonWealthGrantScheme = function () {
    mainTab = tabCommonWealthGrantScheme;
    mainTab.click();
  };
  this.selectTabFeePaying = function () {
    mainTab = tabFeePaying;
    mainTab.click();
  };
  this.selectTabInternational = function () {
    mainTab = tabInternational;
    mainTab.click();
  };
  this.selectTabOther = function () {
    mainTab = tabOther;
    mainTab.click();
    browser.sleep(2000);
  };
  this.selectTabTotal = function () {
    mainTab = tabTotal;
    mainTab.click();
  };
  this.selectSubTabTeachingPeriodOne = async function () {
    subTab = subTabTeachingPeriodOneLink;
    browser.executeScript('arguments[0].click();', subTab);
    await setTable();
  };
  this.selectSubTabTeachingPeriodTwo = async function () {
    subTab = subTabTeachingPeriodTwoLink;
    browser.executeScript('arguments[0].click();', subTab);
    await setTable();
  };
  this.selectSubTabTeachingPeriodThree = async function () {
    subTab = subTabTeachingPeriodThreeLink;
    browser.executeScript('arguments[0].click();', subTab);
    await setTable();
  };
  this.selectSubTabTotal = async function () {
    subTab = subTabTotal;
    browser.executeScript('arguments[0].click();', subTab);
    await setTable();
  };

  this.setRow = function (uoc1, status1) {
    uoc = uoc1;
    status = status1;

  };

  this.setFacultyHeadCount = async function (value) {
    let facultyInput = `//table[@id='${tableId}']//tr[th[text()='${uoc}']][th[text()='${status}']]//input[contains(@name,'plannedHeadcount')]`;
    let facultyHeadCount = element(by.xpath(facultyInput));
    // facultyHeadCount.clear();
    await browser.executeScript("arguments[0].value=" + value, facultyHeadCount);
  };
  this.editFacultyHeadCount = async function (value) {
    let facultyInput = `//table[@id='${tableId}']//tr[th[text()='${uoc}']][th[text()='${status}']]//input[contains(@name,'plannedHeadcount')]`;
    let facultyHeadCount = element(by.xpath(facultyInput));
    facultyHeadCount.clear();
    await facultyHeadCount.sendKeys(value);
    //await browser.executeScript("arguments[0].value=" + value, facultyHeadCount);
  };
  this.getFacultyHeadCount = async function (callback) {
    let facultyHeadCount = element(by.xpath(`//table[@id='${tableId}']//tr[th[text()='${uoc}']][th[text()='${status}']]//input[contains(@name,'plannedHeadcount')]`));
    let headcount = await facultyHeadCount.getAttribute('value');
    return headcount;
  }
  this.setFacultyEftsl = function (value) {
    let field = element(by.xpath(`//table[@id='${tableId}']//tr[th[text()='${uoc}']][th[text()='${status}']]//input[contains(@name,'plannedEftsl')]`));
    field.clear();
    field.sendKeys(value);
  };

  this.validateFacultyHeadCount = function (val) {

    let facultyHeadCount = element(by.xpath(`//table[@id='${tableId}']//tr[th[text()='${uoc}']][th[text()='${status}']]//input[contains(@name,'plannedHeadcount')]`));
    expect(facultyHeadCount.getAttribute("value")).to.eventually.equal(val);

  }
  this.validateFacultyHeadCountIsReadOnly = function () {

    let facultyHeadCount = element(by.xpath(`//table[@id='${tableId}']//tr[th[text()='${uoc}']][th[text()='${status}']]//input[contains(@name,'plannedHeadcount')]`));
    expect(facultyHeadCount.getAttribute("readonly")).to.eventually.equal('true');

  }
  this.validateFacultyEFTSLIsReadOnly = function () {

    let field = element(by.xpath(`//table[@id='${tableId}']//tr[th[text()='${uoc}']][th[text()='${status}']]//input[contains(@name,'plannedEftsl')]`));
    expect(field.getAttribute("readonly")).to.eventually.equal('true');
  }

  this.clickFlowThruContinuing = function () {
    element(by.xpath(`//div[@id='${destId}']//a[contains(text(),'Flow Through to Continuing')]`)).click();
  };

  this.readChangedValues = async function () {
    await browser.sleep(2000);
    targetEftslTeachingPeriodThreeValue = await targetEftslTeachingPeriodThree.getText();

    targetHCTeachingPeriodThreeValue = await browser.executeScript("return arguments[0].innerText;", targetHCTeachingPeriodThree);
    console.log("Value Read:" + targetHCTeachingPeriodThreeValue);
  };
  this.validateFlowThruChangesTableIsDisplayed = async function () {
    browser.driver.switchTo().activeElement();
    browser.wait(EC.visibilityOf(flowThruChangesTable), 5000);
    expect(flowThruChangesTable.getText()).to.eventually.equal('Flow Through to Continuing');

  };

  this.clickProceedButton = function () {
    console.log('Before clicking on proceed');
    browser.executeScript('arguments[0].click();', proceedButton);
  };
  this.validateHeadCountValue = function () {
    console.log("Expected Value:" + targetHCTeachingPeriodThreeValue);
    // console.log(`//table[@id='${tableId}']//tr[th[text()='${uoc}']][th[text()='${status}']]//input[contains(@name,'plannedHeadcount')]`);
    let facultyHeadCount = element(by.xpath(`//table[@id='${tableId}']//tr[th[text()='${uoc}']][th[text()='${status}']]//input[contains(@name,'plannedHeadcount')]`));
    expect(facultyHeadCount.getAttribute('value')).to.eventually.equal(targetHCTeachingPeriodThreeValue);
  };
  this.validateHeadCountBackGroundColor = function () {
    // console.log(`//table[@id='${tableId}']//tr[th[text()='${uoc}']][th[text()='${status}']]//input[contains(@name,'plannedHeadcount')]`);
    let facultyHeadCount = element(by.xpath(`//table[@id='${tableId}']//tr[th[text()='${uoc}']][th[text()='${status}']]//input[contains(@name,'plannedHeadcount')]`));
    expect(facultyHeadCount.getCssValue('background-color')).to.eventually.equal('rgba(255, 255, 0, 1)');
  };
  this.validateEftslValue = function () {
    console.log('Validating EFTS value' + targetEftslTeachingPeriodThreeValue);
    let field = element(by.xpath(`//table[@id='${tableId}']//tr[th[text()='${uoc}']][th[text()='${status}']]//input[contains(@name,'plannedEftsl')]`));
    expect(field.getAttribute('value')).to.eventually.equal(targetEftslTeachingPeriodThreeValue);
  };
  this.validateEftslBackgroundColor = function () {
    //  console.log("Validating EFTS BG Color");
    let field = element(by.xpath(`//table[@id='${tableId}']//tr[th[text()='${uoc}']][th[text()='${status}']]//input[contains(@name,'plannedEftsl')]`));
    expect(field.getCssValue('background-color')).to.eventually.equal('rgba(255, 255, 0, 1)');
  };
  this.validateSuccessMessage = function (message) {
    const flowThruMessage = element(by.xpath(`//div[@id='${destId}']//div[@id='flow-through-message']`));
    expect(browser.executeScript('return arguments[0].innerText;', flowThruMessage)).to.eventually.equal(message);
  };
  this.validateCorrectTableLoaded = function () {
    expect(element(by.id(`${tableId}`)).isDisplayed()).to.eventually.be.true;

  };

  this.validateLeaveAlertPresent = function () {

    let alertDialog = browser.switchTo().alert();

    alertDialog.accept();

  };

  this.pinCurrentTab = function () {
    //  EC.visibilityOf(pinCurrentTabElement,5000);
    EC.elementToBeClickable(pinCurrentTabElement, 10000);
    browser.executeScript('arguments[0].click();', pinCurrentTabElement);
    //pinCurrentTabElement.click();
  };

  this.validatePinCurrentTabOptionIsNotVisible = function () {
    expect(pinCurrentTabElement.isPresent()).to.eventually.be.false;
  }

  this.getBackGroundColorOfTeachingPeriodTwoTab = function () {

    subTabTeachingPeriodTwoLink.getCssValue('background-color').then(function (val) {
      console.log('Background color is:' + val);
    });
    expect(subTabTeachingPeriodTwoLink.getCssValue('background-color')).to.eventually.equal('rgba(255, 255, 0, 1)');

  };
  this.validateTeachingPeriodOneDisabled = function () {

    expect(subTabTeachingPeriodOne.getAttribute('class')).to.eventually.equal('disabledTab');
  };

  this.clickNextButton = function () {
    nextButton.click();
  };
  this.clickPrevButton = function () {
    prevButton.click();
  };
  this.validateNextButtonIsNotVisible = function () {
    expect(nextButton.isPresent()).to.eventually.be.false;
  };

  this.validateprevButtonIsNotVisible = function () {
    expect(prevButton.isPresent()).to.eventually.be.false;
  };


  this.validateNextButtonContainsNextNumericalProgram = function () {
    nextButton.getAttribute('href').then(function (str) {
      let pgmcode = str.substring(
        str.lastIndexOf("/") + 1,
        str.lastIndexOf("?")
      );
      expect(nextButton.getText()).to.eventually.contain('Next');
      expect(nextButton.getText()).to.eventually.contain(pgmcode);

    })
  }
  this.validatePrevButtonContainsPrevNumericalProgram = function () {
    prevButton.getAttribute('href').then(function (str) {
      let pgmcode = str.substring(
        str.lastIndexOf("/") + 1,
        str.lastIndexOf("?")
      );
      expect(prevButton.getText()).to.eventually.contain('Previous');
      expect(prevButton.getText()).to.eventually.contain(pgmcode);

    })
  }
  this.getPgmCodeOnNextButton = async function () {
    let str = await nextButton.getText();
    pgmCodeOnNextButtonValue = str.substring(str.lastIndexOf(":") + 1).trim();
    return pgmCodeOnNextButtonValue;

  }

  this.getPgmCodeOnPrevButton = async function () {
    let str = await prevButton.getText();
    pgmCodeOnPrevButtonValue = str.substring(str.lastIndexOf(":") + 1).trim();
    return pgmCodeOnPrevButtonValue;

  }
  this.validateProgramCodeOnPageMatchesNextButtonValInPrevScreen = function () {
    validateProgramCode(pgmCodeOnNextButtonValue);
  }

  this.validateProgramCodeOnPageMatchesPrevButtonValInPrevScreen = function () {
    validateProgramCode(pgmCodeOnPrevButtonValue);
  }

  this.validateProgramCodeOnPrevButtonValMatchesPageCodeInPrevScreen = function () {
    expect(this.getPgmCodeOnPrevButton()).to.eventually.equal(pgmCodeOnPageValue);
  }

  this.validateProgramCodeOnNextButtonValMatchesPageCodeInPrevScreen = function () {
    expect(this.getPgmCodeOnNextButton()).to.eventually.equal(pgmCodeOnPageValue);
  }


  this.getProgramCodeOnPage = async function () {
    let str = await pgmCodeOnPage.getText();
    pgmCodeOnPageValue = str.substring(0, str.indexOf("-")).trim();
  }

  function validateProgramCode(val) {
    //let pgmCode= await pgmCodeOnPage.getText();
    //return pgmCode;
    expect(pgmCodeOnPage.getText()).to.eventually.contain(val);
  }

  this.validateErrorMessage = function (message) {
    expect(errorMessage.getText()).to.eventually.contain(message);
  }

  this.clickCancelButton = function () {
    cancelButton.click();
  }
  this.validateCancelPopupAppears = function () {
    expect(cancelPopup.popup.isDisplayed()).to.eventually.be.true;
  }
  this.clickNoButtonOnCancelPopup = function () {
    cancelPopup.noButton.click();
  }
  this.clickYesButtonOnCancelPopup = function () {
    cancelPopup.yesButton.click();
  }
  this.clickSaveButton = async function () {
    saveButton.click();
    console.log("0");
    if (pgmCommentsModal.modal.isDisplayed()) {

      await browser.wait(EC.visibilityOf(pgmCommentsModal.commentsArea), 5000);
      pgmCommentsModal.commentsArea.clear();
      let curr_ts = Date.now();

      pgmCommentsModal.commentsArea.sendKeys("Test Comments From Automation" + curr_ts);
      await pgmCommentsModal.saveButton.click();
      console.log("1");
    }
    console.log("2");
  }

  async function setTable() {

    await subTab.getAttribute('href').then(function (val) {
      destId = val.split('#')[1];
    });

    tableId = 'programtable_' + destId;
    console.log("Table id:" + tableId);
  }
};
module.exports = new ProgramTable();
