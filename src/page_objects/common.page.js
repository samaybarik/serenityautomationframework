'use strict';

const CommonPage = function() {

  var EC = protractor.ExpectedConditions;
  // functions

  this.clickButton = function(buttonName){
    element(by.id(buttonName)).click();
  };

  this.modalDisplay = function(modalName){
    browser.wait(EC.visibilityOf(element(by.id(modalName))), 500);
  };

  this.buttonExists = function(buttonName, className){
    EC.textToBePresentInElementValue($(className), buttonName);
  };

  this.disableBrowserPopUp=function(){
    let JS_DISABLE_UNLOAD_DIALOG = "Object.defineProperty(BeforeUnloadEvent.prototype, 'returnValue', { get:function(){}, set:function(){} });";
    browser.executeScript(JS_DISABLE_UNLOAD_DIALOG);
  }

};

module.exports = new CommonPage();
