
var featsLocation = 'features/**/';
exports.config = {
  directConnect: true,

  framework: 'custom',

  frameworkPath: require.resolve('serenity-js'),

  allScriptsTimeout: 80000,

  specs: [ 'features/**/*.feature' ],
 
  cucumberOpts: {
    require: [ 'features/**/*.js' ],
    format: 'pretty',
  },
  logLevel: 'INFO',
  seleniumAddress: 'http://localhost:4444/wd/hub',
  capabilities: 
  
  {
    browserName: 'chrome',
    chromeOptions: {
      args: [
       // '--verbose',
        // 'incognito',
        // 'disable-extensions',
        // 'show-fps-counter=true'
         '--headless',
        // '--disable-gpu'
        'maximize',
      ],
    },
    // execute tests using 2 browsers running in parallel
    shardTestFiles: false,
    maxInstances: 5,
    
  }
  

  // restartBrowserBetweenTests: true
};
